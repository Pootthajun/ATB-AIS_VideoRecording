﻿Imports System.Windows.Forms
Imports System.IO
Imports ConvertVideoFileWindowService.Org.Mentalis.Files

Public Class ConvertVideoFile
    Public Shared Sub ConvertFile(FileType As ConvertFileType)
        Try
            Dim ini As New IniReader(Application.StartupPath & "\ConvertSetting.ini")
            ini.Section = "VideoFileSetting"
            Dim FileRecorededPath As String = ini.ReadString("FileRecorededPath")
            Dim FileConvertPath As String = ini.ReadString("FileConvertPath") & "\"
            FileConvertPath += DateTime.Now.Year.ToString & "\" & DateTime.Now.Year.ToString & "_" & DateTime.Now.Month.ToString.PadLeft(2, "0") & "\"
            FileConvertPath += DateTime.Now.Year.ToString & "_" & DateTime.Now.Month.ToString.PadLeft(2, "0") & "_" & DateTime.Now.Day.ToString.PadLeft(2, "0") & "\"

            'แยกโฟลเดอร์สำหรับเก็บไฟล์ที่ Convert แล้วเป็น yyyy\yyyy_mm\yyyy_mm_dd
            If IO.Directory.Exists(FileConvertPath) = False Then
                IO.Directory.CreateDirectory(FileConvertPath)
            End If

            For Each f As String In Directory.GetFiles(FileRecorededPath)
                Dim fInfo As New FileInfo(f)
                Dim FileName As String = fInfo.Name.Replace(fInfo.Extension, "")

                Dim ff As New NReco.VideoConverter.FFMpegConverter
                If FileType = ConvertFileType.FLV Then
                    ConvertFileFLV(f, FileConvertPath & FileName, ff)
                End If
                ff = Nothing


                File.SetAttributes(f, FileAttributes.Normal)
                File.Delete(f)
            Next

            ini = Nothing

        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ConvertFileFLV(sourceFile As String, destFile As String, ff As NReco.VideoConverter.FFMpegConverter)
        ff.ConvertMedia(sourceFile, destFile & ".flv", NReco.VideoConverter.Format.flv)
    End Sub

    Private Shared Sub ConvertFileMP4(sourceFile As String, destFile As String, ff As NReco.VideoConverter.FFMpegConverter)
        ff.ConvertMedia(sourceFile, destFile & ".mp4", NReco.VideoConverter.Format.flv)
    End Sub

    Private Shared Sub ConvertFileAVI(sourceFile As String, destFile As String, ff As NReco.VideoConverter.FFMpegConverter)
        ff.ConvertMedia(sourceFile, destFile & ".avi", NReco.VideoConverter.Format.avi)
    End Sub
    Private Shared Sub ConvertFileMOV(sourceFile As String, destFile As String, ff As NReco.VideoConverter.FFMpegConverter)
        ff.ConvertMedia(sourceFile, destFile & ".mov", NReco.VideoConverter.Format.mov)
    End Sub
    Private Shared Sub ConvertFileMPG(sourceFile As String, destFile As String, ff As NReco.VideoConverter.FFMpegConverter)
        ff.ConvertMedia(sourceFile, destFile & ".mpg", NReco.VideoConverter.Format.mpeg)
    End Sub

    Public Shared Sub DeleteVideoFile()
        Try
            Dim ini As New IniReader(Application.StartupPath & "\ConvertSetting.ini")
            ini.Section = "VideoFileSetting"
            Dim FileConvertPath As String = ini.ReadString("FileConvertPath")
            Dim ClearFileMonth As String = ini.ReadString("ClearFileMonth")
            If ClearFileMonth.Trim = "" Then
                ClearFileMonth = "3"
            End If

            Dim dtDir As New DataTable
            dtDir.Columns.Add("directory_name")

            'แยกโฟลเดอร์สำหรับเก็บไฟล์ที่ Convert แล้วเป็น yyyy\yyyy_mm\yyyy_mm_dd
            For Each dirYear As String In IO.Directory.GetDirectories(FileConvertPath)
                'Directory ของ ปี  yyyy\

                For Each dirMonth As String In IO.Directory.GetDirectories(dirYear)
                    'Directory ของ เดือน   yyyy\yyyy_mm

                    For Each dirDay As String In IO.Directory.GetDirectories(dirMonth)
                        Dim DirInfo As New IO.DirectoryInfo(dirDay)
                        Dim TmpDate() As String = DirInfo.Name.Split("_")
                        Dim fldDate As New DateTime(TmpDate(0), TmpDate(1), TmpDate(2))

                        If fldDate.AddMonths(Convert.ToInt16(ClearFileMonth)) <= DateTime.Now.Date Then
                            Dim drDir As DataRow = dtDir.NewRow
                            drDir("directory_name") = dirDay
                            dtDir.Rows.Add(drDir)
                        End If
                    Next
                Next
            Next

            If dtDir.Rows.Count > 0 Then
                For Each dr As DataRow In dtDir.Rows
                    If IO.Directory.Exists(dr("directory_name")) = True Then
                        Try
                            IO.Directory.Delete(dr("directory_name"), True)
                        Catch ex As Exception

                        End Try
                    End If

                Next
            End If

            ini = Nothing
            dtDir.Dispose()
        Catch ex As Exception

        End Try
    End Sub


    Enum ConvertFileType
        FLV = 1
        MP4 = 2
        AVI = 3
        MOV = 4
        MPG = 5
    End Enum
End Class

