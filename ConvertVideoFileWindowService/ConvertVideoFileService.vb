﻿Imports System.Windows.Forms
Imports ConvertVideoFileWindowService.Org.Mentalis.Files
Imports System.Timers


Public Class ConvertVideoFileService
    Dim TimerConvertFile As New System.Timers.Timer
    Dim TimerClearFile As New System.Timers.Timer
    Dim ini As New IniReader(Application.StartupPath & "\ConvertSetting.ini")
    Dim FileConvertPath As String = ""
    Dim ConvertFileExtension As String = ""
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        ini.Section = "VideoFileSetting"
        FileConvertPath = ini.ReadString("FileConvertPath") & "\"
        If IO.Directory.Exists(FileConvertPath) = False Then
            IO.Directory.CreateDirectory(FileConvertPath)
        End If

        ConvertFileExtension = ini.ReadString("ConvertFileExtension")


        TimerConvertFile = New System.Timers.Timer
        TimerConvertFile.Interval = 60000
        AddHandler TimerConvertFile.Elapsed, AddressOf TimerConvertFile_Tick
        TimerConvertFile.Start()
        TimerConvertFile.Enabled = True

        TimerClearFile = New System.Timers.Timer
        TimerClearFile.Interval = 60000 * 60   '1 ชั่วโมง 
        AddHandler TimerClearFile.Elapsed, AddressOf TimerClearFile_Tick
        TimerClearFile.Start()
        TimerClearFile.Enabled = True

    End Sub

    Protected Sub TimerClearFile_Tick(sender As Object, e As ElapsedEventArgs)
        ConvertVideoFile.DeleteVideoFile()
    End Sub

    Protected Sub TimerConvertFile_Tick(sender As Object, e As ElapsedEventArgs)
        TimerConvertFile.Enabled = False
        If ConvertFileExtension.ToUpper = "FLV" Then
            ConvertVideoFile.ConvertFile(ConvertVideoFile.ConvertFileType.FLV)
        ElseIf ConvertFileExtension.ToUpper = "MP4" Then
            ConvertVideoFile.ConvertFile(ConvertVideoFile.ConvertFileType.MP4)
        ElseIf ConvertFileExtension.ToUpper = "AVI" Then
            ConvertVideoFile.ConvertFile(ConvertVideoFile.ConvertFileType.AVI)
        ElseIf ConvertFileExtension.ToUpper = "MOV" Then
            ConvertVideoFile.ConvertFile(ConvertVideoFile.ConvertFileType.MOV)
        ElseIf ConvertFileExtension.ToUpper = "MPG" Then
            ConvertVideoFile.ConvertFile(ConvertVideoFile.ConvertFileType.MPG)
        End If
        TimerConvertFile.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
