﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTempVideoRedording
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVideoRedording))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnStartVideoCapture = New System.Windows.Forms.Button()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.lstVideo = New System.Windows.Forms.ListBox()
        Me.lstAudio = New System.Windows.Forms.ListBox()
        Me.btnCaptureFrame = New System.Windows.Forms.Button()
        Me.TimerCheckCapture = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Location = New System.Drawing.Point(3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(461, 293)
        Me.Panel1.TabIndex = 0
        '
        'btnStartVideoCapture
        '
        Me.btnStartVideoCapture.BackColor = System.Drawing.Color.DarkSlateBlue
        Me.btnStartVideoCapture.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStartVideoCapture.ForeColor = System.Drawing.Color.White
        Me.btnStartVideoCapture.Location = New System.Drawing.Point(375, 297)
        Me.btnStartVideoCapture.Name = "btnStartVideoCapture"
        Me.btnStartVideoCapture.Size = New System.Drawing.Size(89, 38)
        Me.btnStartVideoCapture.TabIndex = 2
        Me.btnStartVideoCapture.Text = "START"
        Me.btnStartVideoCapture.UseVisualStyleBackColor = False
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Icon = CType(resources.GetObject("NotifyIcon1.Icon"), System.Drawing.Icon)
        Me.NotifyIcon1.Text = "Video Recording"
        Me.NotifyIcon1.Visible = True
        '
        'lstVideo
        '
        Me.lstVideo.FormattingEnabled = True
        Me.lstVideo.Location = New System.Drawing.Point(470, 2)
        Me.lstVideo.Name = "lstVideo"
        Me.lstVideo.Size = New System.Drawing.Size(254, 160)
        Me.lstVideo.TabIndex = 3
        '
        'lstAudio
        '
        Me.lstAudio.FormattingEnabled = True
        Me.lstAudio.Location = New System.Drawing.Point(470, 174)
        Me.lstAudio.Name = "lstAudio"
        Me.lstAudio.Size = New System.Drawing.Size(254, 160)
        Me.lstAudio.TabIndex = 4
        '
        'btnCaptureFrame
        '
        Me.btnCaptureFrame.Location = New System.Drawing.Point(261, 297)
        Me.btnCaptureFrame.Name = "btnCaptureFrame"
        Me.btnCaptureFrame.Size = New System.Drawing.Size(86, 37)
        Me.btnCaptureFrame.TabIndex = 5
        Me.btnCaptureFrame.Text = "CaptureFrame"
        Me.btnCaptureFrame.UseVisualStyleBackColor = True
        '
        'TimerCheckCapture
        '
        Me.TimerCheckCapture.Enabled = True
        Me.TimerCheckCapture.Interval = 1000
        '
        'frmVideoRedording
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 336)
        Me.Controls.Add(Me.btnCaptureFrame)
        Me.Controls.Add(Me.lstAudio)
        Me.Controls.Add(Me.lstVideo)
        Me.Controls.Add(Me.btnStartVideoCapture)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmVideoRedording"
        Me.Text = "frmVideoRedording"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Private WithEvents btnStartVideoCapture As Button
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents lstVideo As ListBox
    Friend WithEvents lstAudio As ListBox
    Friend WithEvents btnCaptureFrame As Button
    Friend WithEvents TimerCheckCapture As Timer
End Class
