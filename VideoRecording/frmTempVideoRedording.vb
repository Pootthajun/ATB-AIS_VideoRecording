﻿Imports DirectX.Capture
Imports DShowNET

Public Class frmTempVideoRedording
    Dim capture1 As Capture
    Dim filters As Filters

    Dim deviceNumber As Int16 = 0
    Dim RecordFileName As String = Application.StartupPath & "\RecordingFile.avi"

    Private Sub frmVideoRedording_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        filters = New Filters
        If filters.VideoInputDevices IsNot Nothing Then
            Try
                'capture1 = New Capture(filters.VideoInputDevices(deviceNumber), filters.AudioInputDevices(0))
                capture1 = New Capture(filters.VideoInputDevices(deviceNumber), Nothing)

                'startOrStopCapturing(capture1)

                SetCompressVideoList()
                SetCompressAudioList()

                CreateContextMenu()
                'Define RefreshImage as event handler of FrameCaptureComplete
                AddHandler capture1.FrameCaptureComplete, AddressOf SaveCaptureImage

                preview()
            Catch ex As Exception
                MessageBox.Show("Maybe any other software is already using your WebCam." & vbLf & vbLf & " Error Message: " & vbLf & vbLf + ex.Message)
            End Try

        Else
            btnStartVideoCapture.Enabled = False
            MessageBox.Show("No video device connected to your PC!")

        End If
    End Sub

    Public Sub SaveCaptureImage(ByVal Frame As System.Windows.Forms.PictureBox)
        Dim CaptureDir As String = Application.StartupPath & "\ImageCapture\"
        If IO.Directory.Exists(CaptureDir) = False Then
            IO.Directory.CreateDirectory(CaptureDir)
        End If
        Dim Img As Image = Frame.Image
        Img.Save(CaptureDir & DateTime.Now.ToString("yyyyMMddHHmmssfff") & ".png")
    End Sub

    Private Sub SetCompressVideoList()
        Me.lstVideo.Items.Clear()
        For i = 0 To filters.VideoCompressors.Count - 1
            Me.lstVideo.Items.Add(filters.VideoCompressors(i).Name)
        Next
    End Sub
    Private Sub SetCompressAudioList()
        lstAudio.Items.Clear()
        For i = 0 To filters.AudioCompressors.Count - 1
            lstAudio.Items.Add(filters.AudioCompressors(i).Name)
        Next
    End Sub


    Private Sub preview()
        Try
            capture1.PreviewWindow = Panel1

            If btnStartVideoCapture.Text = "STOP" Then
                If Not capture1.Cued Then
                    If (IO.File.Exists(RecordFileName) = True) Then
                        IO.File.SetAttributes(RecordFileName, IO.FileAttributes.Normal)
                        IO.File.Delete(RecordFileName)
                    End If

                    capture1.Filename = RecordFileName
                End If
                capture1.Cue()
            End If

            capture1.Start()
        Catch
        End Try
    End Sub

    Private Sub btnStartVideoCapture_Click(sender As Object, e As EventArgs) Handles btnStartVideoCapture.Click
        startOrStopCapturing(capture1)
    End Sub

    Private Sub startOrStopCapturing(capture As Capture)
        btnStartVideoCapture.Visible = False

        If capture IsNot Nothing Then
            capture.[Stop]()
        End If

        If btnStartVideoCapture.Text = "START" Then
            capture1.FrameRate = 15
            capture1.VideoCompressor = filters.VideoCompressors(lstVideo.SelectedIndex)
            'capture1.AudioCompressor = filters.AudioCompressors(lstAudio.SelectedIndex)

            btnStartVideoCapture.Text = "STOP"
            btnStartVideoCapture.BackColor = Color.Maroon

            Try
                If Not capture.Cued Then
                    If (IO.File.Exists(RecordFileName) = True) Then
                        IO.File.SetAttributes(RecordFileName, IO.FileAttributes.Normal)
                        IO.File.Delete(RecordFileName)
                    End If

                    capture.Filename = RecordFileName
                End If

                capture.Cue()
                capture.Start()
            Catch ex As Exception
                MessageBox.Show("Error Message: " & vbLf & vbLf + ex.Message)
            End Try
        Else
            btnStartVideoCapture.Text = "START"
            btnStartVideoCapture.BackColor = Color.DarkSlateBlue

            If IO.File.Exists(RecordFileName) = True Then
                Dim FileConvert As String = Application.StartupPath & "\FileConvert\"
                If IO.Directory.Exists(FileConvert) = False Then
                    IO.Directory.CreateDirectory(FileConvert)
                End If

                'Dim ff As New NReco.VideoConverter.FFMpegConverter
                'ff.ConvertMedia(RecordFileName, FileConvert & "ConvertVDO.flv", NReco.VideoConverter.Format.flv)
                'ff.ConvertMedia(RecordFileName, FileConvert & "ConvertVDO.avi", NReco.VideoConverter.Format.avi)
                'ff.ConvertMedia(RecordFileName, FileConvert & "ConvertVDO.mp4", NReco.VideoConverter.Format.mp4)
                'ff.ConvertMedia(RecordFileName, FileConvert & "ConvertVDO.mpg", NReco.VideoConverter.Format.mpeg)
                'ff.ConvertMedia(RecordFileName, FileConvert & "ConvertVDO.mov", NReco.VideoConverter.Format.mov)
            End If
        End If
        btnStartVideoCapture.Visible = True
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False

    End Sub


    Public Sub CreateContextMenu()

        'Define New Context Menu and Menu Item 
        Dim contextMenu As New ContextMenu
        Dim menuStop As New MenuItem("Stop")
        contextMenu.MenuItems.Add(menuStop)

        ' Associate context menu with Notify Icon 
        NotifyIcon1.ContextMenu = contextMenu

        'Add functionality for menu Item click 
        AddHandler menuStop.Click, AddressOf menuStop_Click

    End Sub

    Private Sub menuStop_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        startOrStopCapturing(capture1)
    End Sub

    Private Sub frmVideoRedording_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            NotifyIcon1.Visible = True
        Else
            NotifyIcon1.Visible = False
        End If

    End Sub

    Private Sub btnCaptureFrame_Click(sender As Object, e As EventArgs) Handles btnCaptureFrame.Click
        'Dim Folder As String = "D:\MyProject\TIT_Tech\ATB-AIS\SourceCode\VideoRecording\VideoRecording\bin\Debug\FileConvert\"
        'Dim FileOriginal As String = "ConvertVDO.mov"

        'Dim ff As New NReco.VideoConverter.FFMpegConverter
        'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.flv", NReco.VideoConverter.Format.flv)
        'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.avi", NReco.VideoConverter.Format.avi)
        'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mp4", NReco.VideoConverter.Format.mp4)
        'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mpg", NReco.VideoConverter.Format.mpeg)
        'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mov", NReco.VideoConverter.Format.mov)

        'MessageBox.Show("OK")

        capture1.CaptureFrame()
    End Sub
End Class