﻿Imports DirectX.Capture
'Imports DShowNET
Imports VideoRecording.Org.Mentalis.Files
Imports System.IO
Imports System.Drawing.Graphics
Imports System.ComponentModel

Public Class frmVideoRedording
    Dim capture1 As Capture
    Dim filters As Filters

    'Dim deviceNumber As Int16 = 0
    Dim RecordFileName As String = ""
    Dim CurrTransID As String = Application.StartupPath & "\CurrTransID.txt"
    Dim KO_ID As Long = 0

    Dim ws As New KMService.ServerDBService
    Dim ini As New IniReader(Application.StartupPath & "\VideoSetting.ini")

    Private Sub DeleteTempFile()
        For Each f As String In Directory.GetFiles(Application.StartupPath, "*.avi")
            Try
                File.SetAttributes(f, FileAttributes.Normal)
                File.Delete(f)
            Catch ex As Exception

            End Try
        Next
    End Sub

    Private Sub frmVideoRedording_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        DeleteTempFile()

        If File.Exists(Application.StartupPath & "\KO_ID.txt") = True Then
            KO_ID = File.ReadAllText(Application.StartupPath & "\KO_ID.txt")

            filters = New Filters
            If filters.VideoInputDevices IsNot Nothing Then
                Try
                    ini.Section = "Setting"
                    capture1 = New Capture(filters.VideoInputDevices(ini.ReadString("DeviceNumber")), Nothing)
                    'Define RefreshImage as event handler of FrameCaptureComplete
                    'AddHandler capture1.FrameCaptureComplete, AddressOf SaveCaptureImage

                    startRecording()

                    'TimerCaptureImage.Interval = Convert.ToInt16(ini.ReadString("CaptureSetting", "CaptureIntervalMillesec"))

                    'ws.Url = ini.ReadString("Setting", "KMServiceURL")
                    'ws.Timeout = 1000
                    Me.WindowState = FormWindowState.Minimized
                    Me.Hide()
                Catch ex As Exception
                    'MessageBox.Show("Maybe any other software is already using your WebCam." & vbLf & vbLf & " Error Message: " & vbLf & vbLf + ex.Message)
                    CreateErrorLog("frmVideoRedording_Shown", "Maybe any other software is already using your WebCam." & vbLf & vbLf & " Error Message: " & vbLf & vbLf + ex.Message)
                End Try
            Else
                'Error Save Log
            End If
            'Me.Hide()
        Else
            Application.Exit()
        End If
    End Sub

#Region "Video Recording"

    Private Sub startRecording()
        Try
            If File.Exists(CurrTransID) = True Then
                RecordFileName = Application.StartupPath & "\" & File.ReadAllText(CurrTransID) & ".avi"
                If RecordFileName.Trim = "" Then
                    RecordFileName = Application.StartupPath & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfff") & ".avi"
                End If

                ini.Section = "Setting"
                capture1.FrameRate = Convert.ToDouble(ini.ReadString("VideoFrameRate"))
                'capture1.VideoCompressor = filters.VideoCompressors(ini.ReadString("VideoCompressorsIndex"))
                'capture1.AudioCompressor = filters.AudioCompressors(lstAudio.SelectedIndex)

                'Image Capture Section
                ini.Section = "CaptureSetting"
                capture1.FrameSize = New Size(ini.ReadString("ImageWidth"), ini.ReadString("ImageHight"))   'ขนาดของ Frame VDO

                Try
                    If Not capture1.Cued Then
                        If (IO.File.Exists(RecordFileName) = True) Then
                            IO.File.SetAttributes(RecordFileName, IO.FileAttributes.Normal)
                            IO.File.Delete(RecordFileName)
                        End If

                        capture1.Filename = RecordFileName
                    End If

                    capture1.PreviewWindow = Panel1
                    capture1.Cue()
                    capture1.Start()
                Catch ex As Exception
                    'Error เก็บ log
                    'MessageBox.Show("Error Message: " & vbLf & vbLf + ex.Message)
                    CreateErrorLog("startRecording", "Exception : " & ex.Message & vbNewLine & ex.StackTrace)
                End Try

                'Else
                '    RecordFileName = Application.StartupPath & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfff") & ".avi"
            End If
        Catch ex As Exception
            CreateErrorLog("startRecording", "Exception 2 : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub


    Private Sub stopRecording()
        If capture1 IsNot Nothing Then
            capture1.[Stop]()
        End If
    End Sub

    Private Sub MoveFileToConvert(StopRecordFileName As String, NewFileName As String)
        If File.Exists(StopRecordFileName) = True Then
            'Dim rInfo As New FileInfo(StopRecordFileName)
            Dim nInfo As New FileInfo(NewFileName)

            Dim FileRecoreded As String = Application.StartupPath & "\FileRecoreded\"
            If Directory.Exists(FileRecoreded) = False Then
                Directory.CreateDirectory(FileRecoreded)
            End If

            Try
                'Dim SaveFileName As String = rInfo.Name.Replace(rInfo.Extension, "") & "_" & nInfo.Name.Replace(nInfo.Extension, "") & rInfo.Extension
                Dim SaveFileName As String = nInfo.Name

                File.Copy(StopRecordFileName, FileRecoreded & SaveFileName, True)
                File.Delete(StopRecordFileName)
            Catch ex As Exception
                'MessageBox.Show(ex.Message & vbNewLine & ex.StackTrace)
                CreateErrorLog("MoveFileToConvert", "Exception : " & ex.Message & vbNewLine & ex.StackTrace & vbNewLine & "StopRecordFileName=" & StopRecordFileName & vbNewLine & "NewFileName=" & NewFileName)
            End Try
            nInfo = Nothing
        End If
    End Sub

#End Region

    '#Region "Save Capture Image"

    '    Public Sub SaveCaptureImage(ByVal Frame As PictureBox)
    '        Try
    '            System.Net.ServicePointManager.ServerCertificateValidationCallback =
    '                              Function(se As Object, cert As System.Security.Cryptography.X509Certificates.X509Certificate,
    '                              chain As System.Security.Cryptography.X509Certificates.X509Chain,
    '                              sslerror As System.Net.Security.SslPolicyErrors) True


    '            ini.Section = "CaptureSetting"
    '            Dim Img As Image = ResizeImage(Frame.Image, New Size(ini.ReadString("ImageWidth"), ini.ReadString("ImageHight")))

    '            Dim converter As New ImageConverter
    '            Dim b() As Byte = converter.ConvertTo(Img, GetType(Byte()))

    '            Dim ret As Boolean = ws.Set_Camera_Frame(KO_ID, DateTime.Now, "Image/JPEG", b)   '.Set_Camera_Frame(KO_ID, DateTime.Now, "JPG", b)
    '            'If ret = False Then
    '            '    Dim aaa = ret
    '            'End If
    '            If ini.ReadString("SaveImageToFile") = "Y" Then
    '                Dim CaptureDir As String = Application.StartupPath & "\ImageCapture\"
    '                If IO.Directory.Exists(CaptureDir) = False Then
    '                    IO.Directory.CreateDirectory(CaptureDir)
    '                End If

    '                Img.Save(CaptureDir & DateTime.Now.ToString("yyyyMMddHHmmssfff") & ".jpg", Imaging.ImageFormat.Jpeg)
    '            End If
    '        Catch ex As AccessViolationException
    '            CreateErrorLog("SaveCaptureImage", "AccessViolationException : " & ex.Message & vbNewLine & ex.StackTrace)
    '        Catch ex As Exception
    '            If ex.Message.Trim <> "The operation has timed out" Then
    '                'ถ้าแค่ Time out ก็ไม่ต้องเขียน Log  (มันจะเยอะ)

    '                CreateErrorLog("SaveCaptureImage", "Exception : " & ex.Message & vbNewLine & ex.StackTrace)
    '            End If

    '        End Try
    '    End Sub

    '    Private Function ResizeImage(ByVal OldImage As Image, ByVal NewSize As Size) As Image
    '        Dim newWidth As Integer = NewSize.Width
    '        Dim newHeight As Integer = NewSize.Height
    '        Dim newImage As Image = New Bitmap(newWidth, newHeight)


    '        Using graphicsHandle As Graphics = Graphics.FromImage(newImage)
    '            Try
    '                graphicsHandle.InterpolationMode = Drawing2D.InterpolationMode.HighQualityBicubic
    '                graphicsHandle.DrawImage(OldImage, 0, 0, newWidth, newHeight)
    '            Catch ex As AccessViolationException
    '                CreateErrorLog("ResizeImage", "AccessViolationException : " & ex.Message & vbNewLine & ex.StackTrace)
    '            Catch ex As Exception
    '                CreateErrorLog("ResizeImage", "Exception : " & ex.Message & vbNewLine & ex.StackTrace)
    '            End Try
    '        End Using
    '        Return newImage
    '    End Function

    '#End Region

    Private Sub NotifyIcon1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        NotifyIcon1.Visible = False

    End Sub

    Private Sub frmVideoRedording_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            NotifyIcon1.Visible = True
        Else
            NotifyIcon1.Visible = False
        End If

    End Sub

    'Private Sub btnCaptureFrame_Click(sender As Object, e As EventArgs)
    '    'Dim Folder As String = "D:\MyProject\TIT_Tech\ATB-AIS\SourceCode\VideoRecording\VideoRecording\bin\Debug\FileConvert\"
    '    'Dim FileOriginal As String = "ConvertVDO.mov"

    '    'Dim ff As New NReco.VideoConverter.FFMpegConverter
    '    'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.flv", NReco.VideoConverter.Format.flv)
    '    'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.avi", NReco.VideoConverter.Format.avi)
    '    'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mp4", NReco.VideoConverter.Format.mp4)
    '    'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mpg", NReco.VideoConverter.Format.mpeg)
    '    'ff.ConvertMedia(RecordFileName, Folder & "DoubleConvertVDO.mov", NReco.VideoConverter.Format.mov)

    '    'MessageBox.Show("OK")

    '    capture1.CaptureFrame()
    'End Sub

    'Dim CutVideoMin As Int16 = ini.ReadString("Setting", "CutVideoMin")
    'Dim CutVideoTime As DateTime = DateTime.Now
    Dim CurrTransTime As Boolean = False
    Private Sub TimerCheckCapture_Tick(sender As Object, e As EventArgs) Handles TimerCheckCapture.Tick
        TimerCheckCapture.Enabled = False
        CheckShutdownProcess()

        'ตรวจสอบข้อมูลจาก Webservice ว่ามีการสั่งให้ Kiosk เริ่มทำการ ส่งภาพไปใหักับ WebService แล้วหรือไม่
        'Dim IsCapture = (ini.ReadString("Setting", "StartLiveView") = 1)  '' False  'CallWebService
        'If IsCapture = True Then
        '    TimerCaptureImage.Enabled = True
        'Else
        '    TimerCaptureImage.Enabled = False
        'End If


        ''ตรวจสอบเวลาสำหรับการตัดไฟล์ Video
        'If CutVideoTime.AddMinutes(CutVideoMin) <= DateTime.Now Then
        '    Dim CurrFileName As String = RecordFileName
        '    stopRecording()
        '    startRecording()
        '    MoveFileToConvert(CurrFileName, RecordFileName)
        '    CutVideoMin = ini.ReadString("Setting", "CutVideoMin")
        '    CutVideoTime = DateTime.Now
        'End If

        If CurrTransTime = False Then
            If File.Exists(CurrTransID) = True Then
                'ถ้ามีการเริ่มทำ Transaction ไฟล์ CurrTransID.txt จะถูก Create
                Dim CurrFileName As String = RecordFileName
                stopRecording()
                startRecording()
                MoveFileToConvert(CurrFileName, RecordFileName)
                CurrTransTime = True
            End If

            ''ตรวจสอบเวลาสำหรับการตัดไฟล์ Video
            'If CutVideoTime.AddMinutes(CutVideoMin) <= DateTime.Now Then
            '    Dim CurrFileName As String = RecordFileName
            '    stopRecording()
            '    startRecording()
            '    MoveFileToConvert(CurrFileName, RecordFileName)
            '    CutVideoMin = ini.ReadString("Setting", "CutVideoMin")
            '    CutVideoTime = DateTime.Now
            'End If
        Else
            If File.Exists(CurrTransID) = False Then
                'จบ Transaction ไฟล์ CurrTransID.txt จะถูก Delete
                Dim CurrFileName As String = RecordFileName
                stopRecording()
                'startRecording()
                MoveFileToConvert(CurrFileName, RecordFileName)

                'CutVideoMin = ini.ReadString("Setting", "CutVideoMin")
                'CutVideoTime = DateTime.Now
                CurrTransTime = False
            End If
        End If
        TimerCheckCapture.Enabled = True
    End Sub

    Private Sub CheckShutdownProcess()
        Try
            Dim StopFile As String = Application.StartupPath & "\ShutdownFlag.txt"
            Dim StopFlag As Boolean = False
            If File.Exists(StopFile) = True Then
                StopFlag = (File.ReadAllText(StopFile) = "1")
            End If

            If StopFlag = True Then
                TimerCaptureImage.Enabled = False

                Dim CurrFileName As String = RecordFileName
                stopRecording()
                MoveFileToConvert(CurrFileName, Application.StartupPath & "\" & DateTime.Now.ToString("yyyyMMddHHmmssfff") & ".avi")

                File.Delete(StopFile)
                Application.Exit()
            End If
        Catch ex As Exception
            CreateErrorLog("CheckShutdownProcess", "Exception : " & ex.Message & vbNewLine & ex.StackTrace)
        End Try
    End Sub

    Private Sub TimerCaptureImage_Tick(sender As Object, e As EventArgs) Handles TimerCaptureImage.Tick
        TimerCaptureImage.Enabled = False
        capture1.CaptureFrame()
        TimerCaptureImage.Enabled = True
    End Sub


    Private Sub CreateErrorLog(FunctionName As String, ByVal TextMsg As String)
        Try
            Dim LogFolder As String = System.Windows.Forms.Application.StartupPath & "\ErrorLog"
            If IO.Directory.Exists(LogFolder) = False Then
                IO.Directory.CreateDirectory(LogFolder)
            End If
            Dim FILE_NAME As String = LogFolder & "\" & DateTime.Now.ToString("yyyyMMddHH") & ".txt"
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss.fff") & " " & FunctionName & vbNewLine & TextMsg & vbNewLine & vbNewLine)
            objWriter.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub frmVideoRedording_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        IO.File.WriteAllText(Application.StartupPath & "\ShutdownFlag.txt", "1")
    End Sub

End Class