﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestVideoRecording
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnStartTrans = New System.Windows.Forms.Button()
        Me.btnStopTrans = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(396, 215)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnStartTrans
        '
        Me.btnStartTrans.Location = New System.Drawing.Point(59, 88)
        Me.btnStartTrans.Name = "btnStartTrans"
        Me.btnStartTrans.Size = New System.Drawing.Size(75, 23)
        Me.btnStartTrans.TabIndex = 1
        Me.btnStartTrans.Text = "StartTrans"
        Me.btnStartTrans.UseVisualStyleBackColor = True
        '
        'btnStopTrans
        '
        Me.btnStopTrans.Enabled = False
        Me.btnStopTrans.Location = New System.Drawing.Point(232, 88)
        Me.btnStopTrans.Name = "btnStopTrans"
        Me.btnStopTrans.Size = New System.Drawing.Size(75, 23)
        Me.btnStopTrans.TabIndex = 2
        Me.btnStopTrans.Text = "StopTrans"
        Me.btnStopTrans.UseVisualStyleBackColor = True
        '
        'frmTestVideoRecording
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(492, 261)
        Me.Controls.Add(Me.btnStopTrans)
        Me.Controls.Add(Me.btnStartTrans)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frmTestVideoRecording"
        Me.Text = "Test Video Recording"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents btnStartTrans As Button
    Friend WithEvents btnStopTrans As Button
End Class
