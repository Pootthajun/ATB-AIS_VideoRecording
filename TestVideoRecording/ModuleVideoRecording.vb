﻿Imports System.Runtime.InteropServices

Namespace VideoRecording
    Module ModuleVideoRecording
        Dim VideoRecordingPath As String = ""

        Public Sub StartRecording(VideoRecordingExe As String, KO_ID As Integer)
            ''p = Process.Start(VideoRecordingPath)
            If IO.File.Exists(VideoRecordingExe) = True Then
                Shell(VideoRecordingExe)
                Dim vInfo As New IO.FileInfo(VideoRecordingExe)

                VideoRecordingPath = vInfo.Directory.FullName

                Try
                    IO.File.Delete(VideoRecordingPath & "\ShutdownFlag.txt")

                    IO.File.WriteAllText(VideoRecordingPath & "\KO_ID.txt", KO_ID.ToString)
                Catch ex As Exception

                End Try
            End If
        End Sub

        'Public Function StartRecordService(TransID As String) As String
        '    Dim ret As String = "false"
        '    Try
        '        'Application.StartupPath & "\CurrTransID.txt"
        '        IO.File.WriteAllText(VideoRecordingPath & "\CurrTransID.txt", TransID)
        '        ret = "true"
        '    Catch ex As Exception
        '        ret = "false|Exception " + ex.Message & vbNewLine & ex.StackTrace & vbNewLine & "Transaction ID=" & TransID
        '    End Try

        '    Return ret
        'End Function

        'Public Function EndRecordService() As String
        '    Dim ret As String = "false"
        '    Dim TransID As String = IO.File.ReadAllText(VideoRecordingPath & "\CurrTransID.txt")
        '    Try
        '        IO.File.SetAttributes(VideoRecordingPath & "\KO_ID.txt", IO.FileAttributes.Normal)
        '        IO.File.Delete(VideoRecordingPath & "\KO_ID.txt")

        '        IO.File.SetAttributes(VideoRecordingPath & "\CurrTransID.txt", IO.FileAttributes.Normal)
        '        IO.File.Delete(VideoRecordingPath & "\CurrTransID.txt")
        '        ret = "true"
        '    Catch ex As Exception
        '        ret = "false|Exception " + ex.Message & vbNewLine & ex.StackTrace & vbNewLine & "Transaction ID=" & TransID
        '    End Try

        '    Return ret
        'End Function

        Public Sub ShutdownRecording()
            If VideoRecordingPath.Trim <> "" Then
                IO.File.WriteAllText(VideoRecordingPath & "\ShutdownFlag.txt", "1")
            End If

        End Sub
    End Module
End Namespace